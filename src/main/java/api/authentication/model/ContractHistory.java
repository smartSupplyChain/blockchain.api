package api.authentication.model;

public class ContractHistory {
    private String txid;
    private String timestamp;
    private String blockchainStatus;
    private String status;

    private String applicationDate;
    private String quantity;
    private String userClient;
    private String companyClient;
    private String merchandise_id;
    private String stock_id;
    private String userSupplier;
    private String companySupplier;

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBlockchainStatus() {
        return blockchainStatus;
    }

    public void setBlockchainStatus(String blockchainStatus) {
        this.blockchainStatus = blockchainStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUserClient() {
        return userClient;
    }

    public void setUserClient(String userClient) {
        this.userClient = userClient;
    }

    public String getCompanyClient() {
        return companyClient;
    }

    public void setCompanyClient(String companyClient) {
        this.companyClient = companyClient;
    }

    public String getMerchandise_id() {
        return merchandise_id;
    }

    public void setMerchandise_id(String merchandise_id) {
        this.merchandise_id = merchandise_id;
    }

    public String getStock_id() {
        return stock_id;
    }

    public void setStock_id(String stock_id) {
        this.stock_id = stock_id;
    }

    public String getUserSupplier() {
        return userSupplier;
    }

    public void setUserSupplier(String userSupplier) {
        this.userSupplier = userSupplier;
    }

    public String getCompanySupplier() {
        return companySupplier;
    }

    public void setCompanySupplier(String companySupplier) {
        this.companySupplier = companySupplier;
    }
}
