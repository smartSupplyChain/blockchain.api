package api.authentication.model;

public class StockHistory {
    private String txid;
    private String timestamp;
    private String status;
    private String userSupplier;
    private String companySupplier;
    private String merchandiseId;
    private String quantity;
    private String applicationDate;
    private String onDelivery;
    private String contractId;
    private String userClient;
    private String companyClient;

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserSupplier() {
        return userSupplier;
    }

    public void setUserSupplier(String userSupplier) {
        this.userSupplier = userSupplier;
    }

    public String getCompanySupplier() {
        return companySupplier;
    }

    public void setCompanySupplier(String companySupplier) {
        this.companySupplier = companySupplier;
    }

    public String getMerchandiseId() {
        return merchandiseId;
    }

    public void setMerchandiseId(String merchandiseId) {
        this.merchandiseId = merchandiseId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getOnDelivery() {
        return onDelivery;
    }

    public void setOnDelivery(String onDelivery) {
        this.onDelivery = onDelivery;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getUserClient() {
        return userClient;
    }

    public void setUserClient(String userClient) {
        this.userClient = userClient;
    }

    public String getCompanyClient() {
        return companyClient;
    }

    public void setCompanyClient(String companyClient) {
        this.companyClient = companyClient;
    }
}
