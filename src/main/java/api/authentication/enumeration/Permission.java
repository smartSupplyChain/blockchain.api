package api.authentication.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Permission {

    ADMIN("admin");

    @Getter
    private final String permission;
}
