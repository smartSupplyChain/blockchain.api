package api.blockchain.configuration;

import api.authentication.enumeration.AuthenticationAuthority;
import api.authentication.enumeration.TokenType;
import api.authentication.model.Token;
import api.authentication.model.User;
import api.authentication.service.JwtService;
import api.blockchain.utils.Util;
import api.dao.TokenDao;
import api.dao.UserDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

import org.hyperledger.fabric.gateway.*;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Map;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@RequiredArgsConstructor
public class PopulateLocalUserConfiguration {

    @Autowired
    UserDao userDao;
    @Autowired
    TokenDao tokenDao;
    @Autowired
    private  JwtService jwtService;
    @Autowired
    private  PasswordEncoder passwordEncoder;

    static {
        System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "true");
    }

    /**
     * helper function for getting connected to the gateway
     */
    public static Gateway connect() throws Exception{
        // Load a file system based wallet for managing identities.
        Path walletPath = Paths.get("wallet");
        Wallet wallet = Wallets.newFileSystemWallet(walletPath);
        // load a CCP
        Path networkConfigPath = Paths.get("..", "..", "test-network", "organizations", "peerOrganizations", "org1.example.com", "connection-org1.yaml");

        Gateway.Builder builder = Gateway.createBuilder();
        builder.identity(wallet, "appUser").networkConfig(networkConfigPath).discovery(true);
        return builder.connect();
    }

    /**
     * connection to blockchain network.
     * create users in blockchain database and HSQLDB
     * create users in local database (HSQLDB)
     */
    @Bean
    public void initConfiguration() throws Exception {
       JSONArray users = new JSONArray();

       // enrolls the admin and registers the user
       try {
           application.java.EnrollAdmin.main(null);
           application.java.RegisterUser.main(null);
       } catch (Exception e) {
           System.err.println(e);
       }

       // connect to the network and invoke the smart contract
       Gateway gateway = connect();
       // get the network and contract
       Network network = gateway.getNetwork(Util.channelName);
       Contract contract = network.getContract(Util.chainCodeName);
       // create users in blockchain databases
       contract.submitTransaction("InitLedger");
       //create users in local data bases(HSQLDB)
       users = new JSONArray(new String(contract.evaluateTransaction("getAllUsers")));
       populateLocalDataBase(users);
   }

    /**
     * persist blockchain users in local database (HSQLDB)
     */
     public void populateLocalDataBase(JSONArray users) throws Exception {
        if(users != null ){
            ObjectMapper mapper = new ObjectMapper();
            for(int i = 0; i<users.length(); i++){
                Map<String,Object> map = mapper.readValue(users.get(i).toString(), Map.class);
                User user = new User();
                user.setLogin(map.get("UserID").toString());
                user.setEmail(map.get("Email").toString());
                user.setPassword(passwordEncoder.encode(map.get("Password").toString()));
                user.setType(map.get("Type").toString());
                user.setCompany(map.get("Entreprise").toString());
                //Authority for authentication
                user.setAuthenticationAuthority(AuthenticationAuthority.ADMIN);

                userDao.save(user);
                var jwtToken = jwtService.generateToken(user);
                saveUserToken(user, jwtToken);
            }
        }
    }

     /**
      * save user token
      * @param user
      * @param jwtToken
      */
     private void saveUserToken(User user, String jwtToken) {
        var token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenDao.save(token);
    }
}