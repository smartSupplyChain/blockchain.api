package api.blockchain.controller;

import api.dto.AuthenticationRequest;
import api.dto.MerchandiseDto;
import api.blockchain.service.MerchandiseService;
import api.dto.RequestReponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blockchain/merchandiseManager/")
@RequiredArgsConstructor
public class MerchandiseController {
    @Autowired
    MerchandiseService merchandiseService;

    @PostMapping("/merchandises")
    public RequestReponse  merchandises(@RequestBody AuthenticationRequest request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            List<MerchandiseDto> merchandisesDto = merchandiseService.getAllMerchandises();
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(merchandisesDto);
            return response;
        }catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

    @PostMapping("/createMerchandise")
    public RequestReponse createMerchandise(@RequestBody MerchandiseDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            merchandiseService.createMerchandise(request.getId());
            response.setSuccess(true);
            response.setMessage("La marchandise : \""+request.getId() + "\" a bien été créée.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @DeleteMapping("/deleteMerchandise/{id}")
    public  RequestReponse deleteMerchandise(@PathVariable String id) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            merchandiseService.deleteMerchandise(id);
            response.setSuccess(true);
            response.setMessage("La marchandise : \""+id + "\" a bien été supprimée.");
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
        return response;
    }
}