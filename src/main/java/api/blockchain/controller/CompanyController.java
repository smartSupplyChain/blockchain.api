package api.blockchain.controller;

import api.dto.AuthenticationRequest;
import api.dto.CompanyDto;
import api.blockchain.service.CompanyService;
import api.dto.RequestReponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blockchain/companyManager/")
@RequiredArgsConstructor
public class CompanyController {
    @Autowired
    CompanyService companyService;

    @PostMapping("/companies")
    public RequestReponse companies(@RequestBody AuthenticationRequest request) throws Exception {
        RequestReponse response = new RequestReponse();
        try{
            List<CompanyDto> companiesDto = companyService.getAllCompanies();
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(companiesDto);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

    @PostMapping("/createCompany")
    public RequestReponse  createCompany(@RequestBody CompanyDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            companyService.createCompany(request.getId());
            response.setSuccess(true);
            response.setMessage("L'entreprise : \""+request.getId() + "\" a bien été créée.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @DeleteMapping("/deleteCompany/{id}")
    public  RequestReponse deleteCompany(@PathVariable String id) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            companyService.deleteCompany(id);
            response.setMessage("L'entreprise : \""+id + "\" a bien été supprimée.");
            response.setSuccess(true);
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
        return response;
    }
}