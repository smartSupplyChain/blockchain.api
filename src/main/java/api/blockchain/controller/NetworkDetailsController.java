package api.blockchain.controller;


import api.blockchain.utils.BlockChainCommandExecutor;
import api.dto.AuthenticationRequest;
import api.dto.NetworkDetailsDto;
import api.dto.RequestReponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/blockchain/blockChainManager/")
@RequiredArgsConstructor
public class NetworkDetailsController {
    @Autowired
    BlockChainCommandExecutor blockChainCommandExecutor;

    @PostMapping("/networkDetails")
    public RequestReponse networkDetails (@RequestBody AuthenticationRequest request){
        RequestReponse response = new RequestReponse();
        try{
            NetworkDetailsDto result = blockChainCommandExecutor.getNetworkDetails();
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(result);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

}