package api.blockchain.controller;

import api.authentication.model.ContractHistory;
import api.blockchain.service.ContractService;
import api.blockchain.service.HistoryService;
import api.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blockchain/contractManager/")
@RequiredArgsConstructor
public class ContractController {
    @Autowired
    ContractService contractService;
    @Autowired
    HistoryService historyService;

    @PostMapping("/contracts")
    public RequestReponse  contracts(@RequestBody AuthenticationRequest request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            List<ContractDto> contractsDto = contractService.getAllContracts();
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(contractsDto);
            return response;
        }catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

    @PostMapping("/createContract")
    public RequestReponse createContract(@RequestBody ContractDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            contractService.createContract(request);
            response.setSuccess(true);
            response.setMessage("Le contrat : \""+request.getId() + "\" a bien été créé.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @PutMapping("/updateContract")
    public RequestReponse updateContract(@RequestBody ContractDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            contractService.updateContract(request);
            response.setSuccess(true);
            response.setMessage("Le contrat : \""+request.getId() + "\" a bien été modifié.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @DeleteMapping("/deleteContract/{id}")
    public  RequestReponse deleteContract(@PathVariable String id) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            contractService.deleteContract(id);
            response.setSuccess(true);
            response.setMessage("Le contrat : \""+id + "\" a bien été supprimé.");
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
        return response;
    }

    @PostMapping("/contractHistory")
    public RequestReponse contractHistory(@RequestBody HistoryRequest historyRequest) throws Exception {
        RequestReponse response = new RequestReponse();
        try{
            List<ContractHistory> history = historyService.getContractHistory(historyRequest.getId());
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(history);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }
}