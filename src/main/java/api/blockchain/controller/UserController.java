package api.blockchain.controller;

import api.authentication.model.UserHistory;
import api.authentication.model.User;
import api.blockchain.service.HistoryService;
import api.dao.UserDao;
import api.dto.AuthenticationRequest;
import api.dto.HistoryRequest;
import api.dto.RequestReponse;
import api.dto.UserDto;
import api.blockchain.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blockchain/userManager/")
@RequiredArgsConstructor
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    HistoryService historyService;
    @Autowired
    UserDao userDao;

    @PostMapping("/userDetails")
    public RequestReponse  userDetails(@RequestBody AuthenticationRequest request){
        RequestReponse response = new RequestReponse();
        try{
            UserDto userDto = userService.getUsersDetails(request.getEmail());
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(userDto);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

    @PostMapping("/users")
    public RequestReponse users(@RequestBody AuthenticationRequest request) throws Exception {
        RequestReponse response = new RequestReponse();
        try{
            List<UserDto> usersDto = userService.getAllUsers();
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(usersDto);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

    @PostMapping("/createUser")
    public RequestReponse createUser(@RequestBody UserDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            userService.createUser(request);
            response.setSuccess(true);
            response.setMessage("L'utilisateur : \""+request.getLogin() + "\" a bien été créé.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @PutMapping("/updateUser")
    public  RequestReponse updateUser(@RequestBody UserDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            userService.updateUser(request);
            response.setSuccess(true);
            response.setMessage("L'utilisateur : \""+request.getLogin() + "\" a bien été modifié.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @DeleteMapping("/deleteUser/{email}")
    public  RequestReponse deleteUser(@PathVariable String email) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            User user = userDao.findByEmail(email).get();
            userService.deleteUser(user);
            response.setSuccess(true);
            response.setMessage("L'utilisateur : \""+ user.getLogin()+ "\" a bien été supprimé.");
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
        return response;
    }

    @PostMapping("/userHistory")
    public RequestReponse userHistory(@RequestBody HistoryRequest historyRequest) throws Exception {
        RequestReponse response = new RequestReponse();
        try{
            List<UserHistory> history = historyService.getUserHistory(historyRequest.getId());
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(history);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }
}