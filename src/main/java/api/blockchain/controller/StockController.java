package api.blockchain.controller;

import api.authentication.model.StockHistory;
import api.blockchain.service.HistoryService;
import api.dto.AuthenticationRequest;
import api.dto.HistoryRequest;
import api.dto.RequestReponse;
import api.dto.StockDto;
import api.blockchain.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blockchain/stockManager/")
@RequiredArgsConstructor
public class StockController {
    @Autowired
    StockService stockService;
    @Autowired
    HistoryService historyService;

    @PostMapping("/stocks")
    public RequestReponse stocks(@RequestBody AuthenticationRequest request) throws Exception {
        RequestReponse response = new RequestReponse();
        try{
            List<StockDto> stoksDto = stockService.getAllStocks();
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(stoksDto);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

    @PostMapping("/createStock")
    public RequestReponse createStock(@RequestBody StockDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            stockService.createStock(request);
            response.setSuccess(true);
            response.setMessage("Le stock : \""+request.getId() + "\" a bien été créé.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @DeleteMapping("/deleteStock/{id}")
    public  RequestReponse deleteStock(@PathVariable String id) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            stockService.deleteStock(id);
            response.setSuccess(true);
            response.setMessage("Le stock : \""+id + "\" a bien été supprimé.");
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
        return response;
    }

    @PutMapping("/updateStock")
    public  RequestReponse updateStock(@RequestBody StockDto request) throws Exception {
        RequestReponse response = new RequestReponse();
        try {
            stockService.updateStock(request);
            response.setSuccess(true);
            response.setMessage("Le stock : \""+request.getId() + "\" a bien été modifié.");
        }
        catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return  response;
        }
        return response;
    }

    @PostMapping("/stockHistory")
    public RequestReponse stockHistory(@RequestBody HistoryRequest historyRequest) throws Exception {
        RequestReponse response = new RequestReponse();
        try{
            List<StockHistory> history = historyService.getStockHistory(historyRequest.getId());
            response.setSuccess(true);
            response.setMessage("");
            response.setContent(history);
            return response;
        } catch (Exception e){
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }
}