package api.blockchain.utils;

import api.dto.NetworkDetailsDto;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Util {
    public static final String channelName = "mychannel";
    public static final String chainCodeName = "basic";
    public static String dateFormat(){
        Locale locale = new Locale("fr", "FR");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        SimpleDateFormat f = new SimpleDateFormat("EEEE");
        Date today = new Date();
        String dayName = f.format(today);
        String time = new SimpleDateFormat("HH:mm:ss").format(today);
        String date = dayName + " " + dateFormat.format(today) + " à " + time;

        return date;
    }

    /***
     * attention, processing of the return as a string returned by the command line
     * if the result of the command line changes this code will no longer work
     */
    public static NetworkDetailsDto getNetworkDetailsAsObject(List<String> outputList){
        NetworkDetailsDto networkDetailsDto = new NetworkDetailsDto();

        //parsing string:  - Up
        String s1 = outputList.get(0).replace("- ","");
        String status = s1.replace(" ","");
        networkDetailsDto.setNetworkStatus(status);

        //parsing string:  - 4 hours
        String duration1 = outputList.get(1).replace("- ","");
        String duration = duration1.replace(" ","");
        networkDetailsDto.setDuration(duration);

        //parsing string:  - "mychannel",
        String channelName1 = outputList.get(2).replace("- ","");
        String channelName2 = channelName1.replace(" ","");
        String channelName3 = channelName2.replace(",","");
        String channelName = channelName3.replace("\"","");
        networkDetailsDto.setChannelName(channelName);

        //parsing string:  - 97
        String blocks1 = outputList.get(3).replace("- ","");
        String blocks = blocks1.replace(" ","");
        networkDetailsDto.setBlockNumber(blocks);

        //parsing string:  - basic,
        String chainCodeName1 = outputList.get(4).replace("- ","");
        String chainCodeName2 = chainCodeName1.replace(" ","");
        String chainCodeName= chainCodeName2.replace(",","");
        networkDetailsDto.setChainCodeName(chainCodeName);

        // parsing string:  - [Org1MSP:true,Org2MSP:true]
        String o1 = outputList.get(5).replace("- ","");
        String o2 = o1.replace(" ","");
        String o3= o2.replace("[","");
        String o4= o3.replace("]","");

        List<String> orgs = new ArrayList<>();
        orgs = Arrays.asList(o4.split(","));
        Map<String, Boolean> res = new HashMap<>();
        for(String s0 :orgs){
            List<String> items = new ArrayList<>();
            items = Arrays.asList(s0.split(":"));
            res.put(items.get(0),Boolean.valueOf(items.get(1)));
            networkDetailsDto.setOrganizations(res);
        }

        return networkDetailsDto;
    }
}
