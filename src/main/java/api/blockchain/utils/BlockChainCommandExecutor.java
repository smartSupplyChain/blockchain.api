package api.blockchain.utils;

import api.dto.NetworkDetailsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component
public class BlockChainCommandExecutor {

    public NetworkDetailsDto getNetworkDetails() throws Exception{
        NetworkDetailsDto result = new NetworkDetailsDto();
        List<String> outputStringList = new ArrayList<>();

        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", "cd /home/ope-sboubaker/fabric/fabric-samples/SBR/R1.0/network;./TestNetworkGUI.sh");
        try {
            Process process = processBuilder.start();
            StringBuilder output = new StringBuilder();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
                outputStringList.add(line);
                System.out.println(line);
            }
            int exitVal = process.waitFor();

            Thread.sleep(1000);

            if (exitVal == 0) {
                if(!output.toString().isEmpty()){
                    result = Util.getNetworkDetailsAsObject(outputStringList);
                }
                return result;
            } else {
                System.out.println("command not found");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //return null;
        throw  new Exception();
    }

}
