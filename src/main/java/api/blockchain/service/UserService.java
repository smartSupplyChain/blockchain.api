package api.blockchain.service;

import api.blockchain.service.helper.UserServiceHelper;
import api.dao.TokenDao;
import api.dao.UserDao;
import api.authentication.enumeration.AuthenticationAuthority;
import api.authentication.enumeration.TokenType;
import api.authentication.model.Token;
import api.authentication.model.User;
import api.authentication.service.JwtService;
import api.dto.UserDto;
import api.blockchain.utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    private UserServiceHelper userServiceHelper;

    @Autowired
    TokenDao tokenDao;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * retrieve user details from Hsqldb
     * @param email
     * @return
     */
    public UserDto getUsersDetails(String email){
        Optional<User> user = userDao.findByEmail(email);
        if(user.isPresent()){
            UserDto userDto = new UserDto();
            userDto.setLogin(user.get().getLogin());
            userDto.setType(user.get().getType());
            userDto.setCompany(user.get().getCompany());
            userDto.setEmail(user.get().getEmail());
         return userDto;
        }
        else return null;
    }

    /**
     * retrieve all users  from blockchain
     * @return
     * @throws Exception
     */
    public List<UserDto> getAllUsers() throws Exception {
        JSONArray users = new JSONArray(userServiceHelper.getAllUsers());
        if(users != null ) {
            List<UserDto> userDtoList = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();

            for (int i = 0; i < users.length(); i++) {
                UserDto userDto = new UserDto();
                Map<String, Object> map = mapper.readValue(users.getString(i), Map.class);
                userDto.setCompany(map.get("Entreprise").toString());
                userDto.setLogin(map.get("UserID").toString());
                userDto.setType(map.get("Type").toString());
                userDto.setEmail(map.get("Email").toString());
                userDtoList.add(userDto);
            }
            return  userDtoList;
        }
        return null;
    }

    /**
     * create user
     * @param userDto
     * @return
     * @throws Exception
     */
    public void createUser(UserDto userDto) throws Exception {
        User user = new User();
        String date = Util.dateFormat();
        user.setLogin(userDto.getLogin());
        user.setCreationDate(date);
        user.setType(userDto.getType());
        user.setCompany(userDto.getCompany());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        // save user in blockchain database
        userServiceHelper.createUser(user);

        // save user with token in local database for authentication
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        //Authority for authentication
        user.setAuthenticationAuthority(AuthenticationAuthority.ADMIN);
        userDao.save(user);
        var jwtToken = jwtService.generateToken(user);
        saveUserToken(user, jwtToken);
    }

    /**
     * updateUser
     * @param userDto
     * @throws Exception
     */
    public void updateUser(UserDto userDto) throws Exception{
        Optional<User> user = userDao.findByEmail(userDto.getEmail());
        if(user.isPresent()){
            // cant's edit email and login because they are considered identifying
            String date = Util.dateFormat();
            user.get().setCreationDate(date);
            user.get().setType(userDto.getType());
            user.get().setCompany(userDto.getCompany());
            user.get().setPassword(userDto.getPassword());
            //update User
            userServiceHelper.updateUser(user.get());

            // save user with token in local database for authentication
            user.get().setPassword(passwordEncoder.encode(userDto.getPassword()));
            userDao.save(user.get());
            var jwtToken = jwtService.generateToken(user.get());
            updateUserToken(user.get(), jwtToken);
        } else {
            throw new Exception("Utilisater n'existe pas dans base locale.");
        }
    }

    /**
     * delete user
     */
    public void deleteUser(User user) throws Exception {
        //delete from blockchain database
        userServiceHelper.deleteUser(user.getLogin());
        //delete from local database(hsqldb)
        userDao.delete(user);
    }

    /**
     * save user token
     * @param user
     * @param jwtToken
     */
    private void saveUserToken(User user, String jwtToken) {
        var token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenDao.save(token);
    }

    private void updateUserToken(User user, String jwtToken) throws Exception {
        Optional<Token> token = tokenDao.findByUser(user);
        if(token.isPresent()){
            token.get().setToken(jwtToken);
            tokenDao.save(token.get());
        }
        else {
            throw new Exception("Token n'existe pas dans la base locale.");
        }
    }
}