package api.blockchain.service;

import api.authentication.model.Stock;
import api.blockchain.service.helper.StockServiceHelper;
import api.dto.StockDto;
import api.blockchain.utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class StockService {

    @Autowired
    private StockServiceHelper stockServiceHelper;

    public List<StockDto> getAllStocks() throws Exception {
        JSONArray stocks = new JSONArray(stockServiceHelper.getAllStocks());
        if(stocks != null ) {
            List<StockDto> stockDtoList = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();

            for (int i = 0; i < stocks.length(); i++) {
                StockDto stockDto = new StockDto();
                Map<String, Object> map = mapper.readValue(stocks.getString(i), Map.class);
                stockDto.setId(map.get("StockID").toString());
                stockDto.setDate(map.get("Datestock").toString());
                stockDto.setQuantity(map.get("Stock").toString());
                stockDto.setOnDelivery(map.get("EnLivraison").toString());
                stockDto.setUserSupplier(map.get("UserFournisseur").toString());
                stockDto.setCompanySupplier(map.get("EntrepriseFournisseur").toString());
                stockDto.setMerchandiseId(map.get("Id_marchandise").toString());
                stockDto.setContractId(map.get("Id_contrat").toString());
                stockDto.setUserClient(map.get("UserClient").toString());
                stockDto.setCompanyClient(map.get("EntrepriseClient").toString());

                stockDtoList.add(stockDto);
            }
           return  stockDtoList;
        }
        return null;
    }

    public Boolean stockExists(String id) throws Exception {
        JSONArray stocks = new JSONArray(stockServiceHelper.getAllStocks());
        if(stocks != null ) {
            ObjectMapper mapper = new ObjectMapper();
            for (int i = 0; i < stocks.length(); i++) {
                Map<String, Object> map = mapper.readValue(stocks.getString(i), Map.class);
                if (map.get("StockID").toString().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void createStock(StockDto stockDto) throws Exception {
        Stock stock = new Stock();
        String date = Util.dateFormat();

        stock.setId(stockDto.getId());
        stock.setDate(date);
        stock.setQuantity(stockDto.getQuantity());
        stock.setOnDelivery("0");
        stock.setUserSupplier(stockDto.getUserSupplier());
        stock.setCompanySupplier(stockDto.getCompanySupplier());
        stock.setMerchandiseId(stockDto.getMerchandiseId());
        stock.setContractId("");
        stock.setUserClient("");
        stock.setCompanyClient("");

        stockServiceHelper.createStock(stock);
    }

    public void updateStock(StockDto stockDto) throws Exception {
        Stock stock = new Stock();
        String date = Util.dateFormat();

        stock.setId(stockDto.getId());
        stock.setDate(date);
        stock.setQuantity(stockDto.getQuantity());
        stock.setOnDelivery("0");
        stock.setUserSupplier(stockDto.getUserSupplier());
        stock.setCompanySupplier(stockDto.getCompanySupplier());
        stock.setMerchandiseId(stockDto.getMerchandiseId());
        stock.setContractId("");
        stock.setUserClient("");
        stock.setCompanyClient("");

        stockServiceHelper.updateStock(stock);
    }

    public void deleteStock(String id) throws Exception {
        stockServiceHelper.deleteStock(id);
    }

}