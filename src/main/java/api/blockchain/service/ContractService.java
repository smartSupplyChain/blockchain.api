package api.blockchain.service;

import api.blockchain.service.helper.ContractServiceHelper;
import api.blockchain.utils.Util;
import api.dto.ContractDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ContractService {

    @Autowired
    private ContractServiceHelper contractServiceHelper;

    /**
     * retrieve contracts  from blockchain
     * @return
     */
    public List<ContractDto> getAllContracts() throws Exception {
        JSONArray contracts = new JSONArray(contractServiceHelper.getAllContracts());
        if(contracts != null ) {
            List<ContractDto> contractDtoList = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();

            for (int i = 0; i < contracts.length(); i++) {
                ContractDto contractDto = new ContractDto();
                Map<String, Object> map = mapper.readValue(contracts.getString(i), Map.class);
                contractDto.setId(map.get("ContratID").toString());
                contractDto.setDate(map.get("Datecontrat").toString());
                contractDto.setStatus(map.get("Statut").toString());
                contractDto.setQuantity(map.get("Quantite").toString());
                contractDto.setUserClient(map.get("UserClient").toString());
                contractDto.setCompanyClient(map.get("EntrepriseClient").toString());
                contractDto.setStock_id(map.get("Id_stock").toString());
                contractDto.setMerchandise_id(map.get("Id_marchandise").toString());
                contractDto.setUserSupplier(map.get("UserFournisseur").toString());
                contractDto.setCompanySupplier(map.get("EntrepriseFournisseur").toString());

                contractDtoList.add(contractDto);
            }
           return  contractDtoList;
        }
        return null;
    }

    /**
     * create contract
     * @param contractDto
     * @return
     */
    public void createContract(ContractDto contractDto) throws Exception {
        String date = Util.dateFormat();
        contractDto.setDate(date);
        contractDto.setStatus("");
        contractDto.setUserSupplier("");
        contractDto.setCompanySupplier("");
        contractDto.setStock_id("");
        contractServiceHelper.createContract(contractDto);
    }

    /**
     * update updateContrat
     * @param contractDto
     * @return
     */
    public void updateContract(ContractDto contractDto) throws Exception {
        String date = Util.dateFormat();
        contractDto.setDate(date);
        contractDto.setStatus(contractDto.getStatus());
        contractDto.setUserSupplier(contractDto.getUserSupplier());
        contractDto.setCompanySupplier(contractDto.getCompanySupplier());
        contractDto.setStock_id(contractDto.getStock_id());
        contractServiceHelper.updateContract(contractDto);
    }

    /**
     * delete contract
     * @param id
     * @throws Exception
     */
    public void deleteContract(String id) throws Exception {
        contractServiceHelper.deleteContract(id);
    }

}