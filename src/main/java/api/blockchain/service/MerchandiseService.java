package api.blockchain.service;

import api.blockchain.service.helper.MerchandiseServiceHelper;
import api.authentication.model.Merchandise;
import api.dto.MerchandiseDto;
import api.blockchain.utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class MerchandiseService {

    @Autowired
    private MerchandiseServiceHelper merchandiseServiceHelper;

    /**
     * retrieve merchandise  from blockchain
     * @return
     */
    public List<MerchandiseDto> getAllMerchandises() throws Exception {
        JSONArray merchandises = new JSONArray(merchandiseServiceHelper.getAllMerchandises());
        if(merchandises != null ) {
            List<MerchandiseDto> merchandiseDtoList = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();

            for (int i = 0; i < merchandises.length(); i++) {
                MerchandiseDto merchandiseDto = new MerchandiseDto();
                Map<String, Object> map = mapper.readValue(merchandises.getString(i), Map.class);
                merchandiseDto.setId(map.get("MarchandiseID").toString());
                merchandiseDto.setDate(map.get("DateMarchandise").toString());
                merchandiseDtoList.add(merchandiseDto);
            }
           return  merchandiseDtoList;
        }
        return null;
    }

    /**
     * create merchandise
     * @param id
     * @return
     */
    public void createMerchandise(String id) throws Exception {
        Merchandise merchandise = new Merchandise();
        String date = Util.dateFormat();
        merchandise.setId(id);
        merchandise.setDate(date);
        merchandiseServiceHelper.createMerchandise(merchandise);
    }

    /**
     * delete merchandise
     * @param id
     * @throws Exception
     */
    public void deleteMerchandise(String id) throws Exception {
        merchandiseServiceHelper.deleteMerchandise(id);
    }

}