package api.blockchain.service.helper;

import api.blockchain.utils.Util;
import api.dto.ContractDto;
import lombok.RequiredArgsConstructor;
import org.hyperledger.fabric.gateway.*;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
@Component
public class ContractServiceHelper {

    static {
        System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "true");
    }

    /**
     * helper function for getting connected to the gateway
     */
    public static Gateway connect() throws Exception{
        // Load a file system based wallet for managing identities.
        Path walletPath = Paths.get("wallet");
        Wallet wallet = Wallets.newFileSystemWallet(walletPath);
        // load a CCP
        Path networkConfigPath = Paths.get("..", "..", "test-network", "organizations", "peerOrganizations", "org1.example.com", "connection-org1.yaml");

        Gateway.Builder builder = Gateway.createBuilder();
        builder.identity(wallet, "appUser").networkConfig(networkConfigPath).discovery(true);
        return builder.connect();
    }

    /**
     * retrieve contracts json from blockchain
     */
    public String getAllContracts() throws Exception {
        String result ="";

        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }

        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.evaluateTransaction("getAllContrats"));
        System.out.println("getAllContrats, result: " + result);

        return result;
    }

    /**
     * create contract
     */
    public void createContract(ContractDto contractDto) throws Exception {
        String result ="";
        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }
        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("createContrat",contractDto.getDate(),contractDto.getId(),contractDto.getStatus(),
                contractDto.getQuantity(),contractDto.getUserClient(),contractDto.getCompanyClient(),contractDto.getMerchandise_id(),contractDto.getStock_id(),contractDto.getUserSupplier(),contractDto.getCompanySupplier()));
        System.out.println("createContract, result: " + result);
    }

    /**
     * update contract
     */
    public void updateContract(ContractDto contractDto) throws Exception {
        String result ="";
        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }

        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("updateContrat",contractDto.getDate(), contractDto.getId(), contractDto.getStatus(),
                contractDto.getQuantity(), contractDto.getUserClient(), contractDto.getCompanyClient(), contractDto.getMerchandise_id(), contractDto.getStock_id(), contractDto.getUserSupplier(), contractDto.getCompanySupplier()));
        System.out.println("updateContract, result: " + result);

    }

    /**
     * delete contract
     */
    public void deleteContract(String id) throws Exception {
        String result ="";
        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }
        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("deleteContrat",id));
        System.out.println("deleteContract, result: " + result);
    }

}
