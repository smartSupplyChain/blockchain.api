package api.blockchain.service.helper;

import api.authentication.model.User;
import api.blockchain.utils.Util;
import lombok.RequiredArgsConstructor;
import org.hyperledger.fabric.gateway.*;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
@Component
public class UserServiceHelper {

    static {
        System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "true");
    }

    /**
     * helper function for getting connected to the gateway
     */
    public static Gateway connect() throws Exception{
        // Load a file system based wallet for managing identities.
        Path walletPath = Paths.get("wallet");
        Wallet wallet = Wallets.newFileSystemWallet(walletPath);
        // load a CCP
        Path networkConfigPath = Paths.get("..", "..", "test-network", "organizations", "peerOrganizations", "org1.example.com", "connection-org1.yaml");

        Gateway.Builder builder = Gateway.createBuilder();
        builder.identity(wallet, "appUser").networkConfig(networkConfigPath).discovery(true);
        return builder.connect();
    }

    /**
     * retrieve users json from blockchain
     */
    public String getAllUsers() throws Exception {
        String result ="";

        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }

        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.evaluateTransaction("getAllUsers"));
        System.out.println("getAllUsers, result: " + result);

        return result;
    }

    /**
     * create user
     */
    public void createUser(User user) throws Exception {
        String result ="";

        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }

        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("createUser",user.getCreationDate(), user.getLogin(), user.getType(),
                user.getCompany(), user.getPassword(), user.getEmail()));
        System.out.println("createUser, result: " + result);
    }

    /**
     * create user
     */
    public void updateUser(User user) throws Exception {
        String result ="";

        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }

        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("updateUser",user.getCreationDate(), user.getLogin(), user.getType(),
                user.getCompany(), user.getPassword(), user.getEmail()));
        System.out.println("updateUser, result: " + result);
    }

    /**
     * delete user
     */
    public void deleteUser(String login) throws Exception {
        String result ="";

        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }

        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("deleteUser",login));
        System.out.println("deleteUser, result: " + result);
    }
}
