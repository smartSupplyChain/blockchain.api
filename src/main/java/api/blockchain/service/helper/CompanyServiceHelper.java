package api.blockchain.service.helper;

import api.authentication.model.Company;
import api.blockchain.utils.Util;
import lombok.RequiredArgsConstructor;
import org.hyperledger.fabric.gateway.*;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
@Component
public class CompanyServiceHelper {

    static {
        System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "true");
    }

    /**
     * helper function for getting connected to the gateway
     */
    public static Gateway connect() throws Exception{
        // Load a file system based wallet for managing identities.
        Path walletPath = Paths.get("wallet");
        Wallet wallet = Wallets.newFileSystemWallet(walletPath);
        // load a CCP
        Path networkConfigPath = Paths.get("..", "..", "test-network", "organizations", "peerOrganizations", "org1.example.com", "connection-org1.yaml");

        Gateway.Builder builder = Gateway.createBuilder();
        builder.identity(wallet, "appUser").networkConfig(networkConfigPath).discovery(true);
        return builder.connect();
    }

    /**
     * retrieve companies json from blockchain
     */
    public String getAllCompanies() throws Exception {
        String result ="";

        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }

        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.evaluateTransaction("getAllEntreprises"));
        System.out.println("getAllCompanies, result: " + result);

        return result;
    }

    /**
     * create company
     */
    public void createCompany(Company company) throws Exception {
        String result ="";
        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }
        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("createEntreprise",company.getDate(), company.getId()));
        System.out.println("createCompany, result: " + result);
    }

    /**
     * delete company
     */
    public void deleteCompany(String id) throws Exception {
        String result ="";
        // enrolls the admin and registers the user
        try {
            application.java.EnrollAdmin.main(null);
            application.java.RegisterUser.main(null);
        } catch (Exception e) {
            System.err.println(e);
        }
        // connect to the network and invoke the smart contract
        Gateway gateway = connect();
        // get the network and contract
        Network network = gateway.getNetwork(Util.channelName);
        Contract contract = network.getContract(Util.chainCodeName);

        result = new String(contract.submitTransaction("deleteEntreprise",id));
        System.out.println("deleteCompany, result: " + result);
    }

}
