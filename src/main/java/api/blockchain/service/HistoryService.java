package api.blockchain.service;

import api.authentication.model.ContractHistory;
import api.authentication.model.StockHistory;
import api.authentication.model.UserHistory;
import api.blockchain.service.helper.HistoryServiceHelper;
import api.blockchain.utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class HistoryService {

    @Autowired
    private HistoryServiceHelper historyServiceHelper;

    public List<UserHistory> getUserHistory(String id) throws Exception {
        historyServiceHelper.getHistory(id);
        JSONArray jsonHistory = new JSONArray(historyServiceHelper.getHistory(id));
        List<UserHistory> userHistoryList = new ArrayList<>();
        if(jsonHistory != null && (jsonHistory.length() > 0 ) ) {
            ObjectMapper mapper = new ObjectMapper();
            for (int i = 0; i < jsonHistory.length(); i++) {
                UserHistory userHistory = new UserHistory();
                Map<String, Object> map = mapper.readValue(jsonHistory.getString(i), Map.class);
                userHistory.setTxid(map.get("txid").toString());
                userHistory.setStatus(""); //TODO how to retrieve this data ?
                userHistory.setTimestamp(map.get("timestamp").toString());

                if(!map.get("value").toString().isEmpty()){
                    Map<String, Object> valueMap = mapper.readValue(map.get("value").toString(), Map.class);
                    userHistory.setApplicationDate(valueMap.get("DateUser").toString());
                    userHistory.setCompany(valueMap.get("Entreprise").toString());
                    userHistory.setProfil(valueMap.get("Type").toString());
                }
                else {
                    userHistory.setApplicationDate("--");
                    userHistory.setCompany("--");
                    userHistory.setProfil("--");
                    userHistory.setStatus("Supprimé");
                }
                userHistoryList.add(userHistory);
            }
        }
        return userHistoryList;
    }

    public List<StockHistory> getStockHistory(String id) throws Exception {
        historyServiceHelper.getHistory(id);
        JSONArray jsonHistory = new JSONArray(historyServiceHelper.getHistory(id));
        List<StockHistory> stockHistoryList = new ArrayList<>();
        if(jsonHistory != null && (jsonHistory.length() > 0 ) ) {
            ObjectMapper mapper = new ObjectMapper();
            for (int i = 0; i < jsonHistory.length(); i++) {
                StockHistory stockHistory = new StockHistory();
                Map<String, Object> map = mapper.readValue(jsonHistory.getString(i), Map.class);
                stockHistory.setTxid(map.get("txid").toString());
                stockHistory.setStatus(""); //TODO how to retrieve this data ?
                stockHistory.setTimestamp(map.get("timestamp").toString());

                if(!map.get("value").toString().isEmpty()){
                    Map<String, Object> valueMap = mapper.readValue(map.get("value").toString(), Map.class);
                    stockHistory.setUserSupplier(valueMap.get("UserFournisseur").toString());
                    stockHistory.setCompanySupplier(valueMap.get("EntrepriseFournisseur").toString());
                    stockHistory.setMerchandiseId(valueMap.get("Id_marchandise").toString());
                    stockHistory.setQuantity(valueMap.get("Stock").toString());
                    stockHistory.setApplicationDate(valueMap.get("Datestock").toString());
                    stockHistory.setOnDelivery(valueMap.get("EnLivraison").toString());
                    stockHistory.setContractId(valueMap.get("Id_contrat").toString());
                    stockHistory.setUserClient(valueMap.get("UserClient").toString());
                    stockHistory.setCompanyClient(valueMap.get("EntrepriseClient").toString());
                }
                else {
                    stockHistory.setUserSupplier("--");
                    stockHistory.setCompanySupplier("--");
                    stockHistory.setMerchandiseId("--");
                    stockHistory.setQuantity("--");
                    stockHistory.setApplicationDate("--");
                    stockHistory.setOnDelivery("--");
                    stockHistory.setContractId("--");
                    stockHistory.setUserClient("--");
                    stockHistory.setCompanyClient("--");
                    stockHistory.setStatus("Supprimé");
                }
                stockHistoryList.add(stockHistory);
            }
        }
        return stockHistoryList;
    }

    public List<ContractHistory> getContractHistory(String id) throws Exception {
        historyServiceHelper.getHistory(id);
        JSONArray jsonHistory = new JSONArray(historyServiceHelper.getHistory(id));
        List<ContractHistory> contractHistoryList = new ArrayList<>();
        if(jsonHistory != null && (jsonHistory.length() > 0 ) ) {
            ObjectMapper mapper = new ObjectMapper();
            for (int i = 0; i < jsonHistory.length(); i++) {
                ContractHistory contractHistory = new ContractHistory();
                Map<String, Object> map = mapper.readValue(jsonHistory.getString(i), Map.class);
                contractHistory.setTxid(map.get("txid").toString());
                contractHistory.setBlockchainStatus("");
                contractHistory.setTimestamp(map.get("timestamp").toString());

                if(!map.get("value").toString().isEmpty()){
                    Map<String, Object> valueMap = mapper.readValue(map.get("value").toString(), Map.class);

                    contractHistory.setApplicationDate(valueMap.get("Datecontrat").toString());
                    contractHistory.setQuantity(valueMap.get("Quantite").toString());
                    contractHistory.setUserClient(valueMap.get("UserClient").toString());
                    contractHistory.setCompanyClient(valueMap.get("EntrepriseClient").toString());
                    contractHistory.setMerchandise_id(valueMap.get("Id_marchandise").toString());
                    contractHistory.setStock_id(valueMap.get("Id_stock").toString());
                    contractHistory.setUserSupplier(valueMap.get("UserFournisseur").toString());
                    contractHistory.setCompanySupplier(valueMap.get("EntrepriseFournisseur").toString());
                    contractHistory.setStatus(valueMap.get("Statut").toString());

                }
                else {
                    contractHistory.setApplicationDate("--");
                    contractHistory.setQuantity("--");
                    contractHistory.setUserClient("--");
                    contractHistory.setCompanyClient("--");
                    contractHistory.setMerchandise_id("--");
                    contractHistory.setStock_id("--");
                    contractHistory.setUserSupplier("--");
                    contractHistory.setCompanySupplier("--");
                    contractHistory.setStatus("--");
                    contractHistory.setBlockchainStatus("Supprimé");
                }
                contractHistoryList.add(contractHistory);
            }
        }
        return contractHistoryList;
    }
}