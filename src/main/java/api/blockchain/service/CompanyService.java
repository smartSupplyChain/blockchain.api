package api.blockchain.service;

import api.blockchain.service.helper.CompanyServiceHelper;
import api.authentication.model.Company;
import api.dto.CompanyDto;
import api.blockchain.utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class CompanyService {

    @Autowired
    private CompanyServiceHelper companyServiceHelper;

    /**
     * retrieve companies  from blockchain
     * @return
     */
    public List<CompanyDto> getAllCompanies() throws Exception {
        JSONArray companies = new JSONArray(companyServiceHelper.getAllCompanies());
        if(companies != null ) {
            List<CompanyDto> companyDtoList = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();

            for (int i = 0; i < companies.length(); i++) {
                CompanyDto companyDto = new CompanyDto();
                Map<String, Object> map = mapper.readValue(companies.getString(i), Map.class);
                companyDto.setId(map.get("EntrepriseID").toString());
                companyDto.setDate(map.get("DateEntreprise").toString());
                companyDtoList.add(companyDto);
            }
           return  companyDtoList;
        }
        return null;
    }

    /**
     * create company
     * @return
     */
    public void createCompany(String id) throws Exception {
        Company company = new Company();
        String date = Util.dateFormat();
        company.setId(id);
        company.setDate(date);
        companyServiceHelper.createCompany(company);
    }

    /**
     * delete company
     * @param id
     * @throws Exception
     */
    public void deleteCompany(String id) throws Exception {
        companyServiceHelper.deleteCompany(id);
    }

}