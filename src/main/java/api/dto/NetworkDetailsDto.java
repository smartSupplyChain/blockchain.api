package api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NetworkDetailsDto {

    @JsonProperty("networkStatus")
    private String networkStatus;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("channelName")
    private String channelName;
    @JsonProperty("blockNumber")
    private String blockNumber;
    @JsonProperty("chainCodeName")
    private String chainCodeName;
    @JsonProperty("organizations")
    private Map<String, Boolean> organizations;

    public String getNetworkStatus() {
        return networkStatus;
    }

    public void setNetworkStatus(String networkStatus) {
        this.networkStatus = networkStatus;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChainCodeName() {
        return chainCodeName;
    }

    public void setChainCodeName(String chainCodeName) {
        this.chainCodeName = chainCodeName;
    }

    public Map<String, Boolean> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(Map<String, Boolean> organizations) {
        this.organizations = organizations;
    }
}
