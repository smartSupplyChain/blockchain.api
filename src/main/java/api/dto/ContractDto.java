package api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContractDto {

    @JsonProperty("id")
    private String id;
    @JsonProperty("date")
    private String date;
    @JsonProperty("status")
    private String status;
    @JsonProperty("quantity")
    private String quantity;
    @JsonProperty("userClient")
    private String userClient;
    @JsonProperty("companyClient")
    private String companyClient;
    @JsonProperty("stock_id")
    private String stock_id;
    @JsonProperty("merchandise_id")
    private String merchandise_id;
    @JsonProperty("userSupplier")
    private String userSupplier;
    @JsonProperty("companySupplier")
    private String companySupplier;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUserClient() {
        return userClient;
    }

    public void setUserClient(String userClient) {
        this.userClient = userClient;
    }

    public String getCompanyClient() {
        return companyClient;
    }

    public void setCompanyClient(String companyClient) {
        this.companyClient = companyClient;
    }

    public String getStock_id() {
        return stock_id;
    }

    public void setStock_id(String stock_id) {
        this.stock_id = stock_id;
    }

    public String getMerchandise_id() {
        return merchandise_id;
    }

    public void setMerchandise_id(String merchandise_id) {
        this.merchandise_id = merchandise_id;
    }

    public String getUserSupplier() {
        return userSupplier;
    }

    public void setUserSupplier(String userSupplier) {
        this.userSupplier = userSupplier;
    }

    public String getCompanySupplier() {
        return companySupplier;
    }

    public void setCompanySupplier(String companySupplier) {
        this.companySupplier = companySupplier;
    }
}