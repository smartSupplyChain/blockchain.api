package api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockDto {

    @JsonProperty("id")
    private String id;
    @JsonProperty("date")
    private String date;
    @JsonProperty("quantity")
    private String quantity;
    @JsonProperty("onDelivery")
    private String onDelivery;
    @JsonProperty("userSupplier")
    private String userSupplier;
    @JsonProperty("companySupplier")
    private String companySupplier;
    @JsonProperty("merchandiseId")
    private String merchandiseId;
    @JsonProperty("contractId")
    private String contractId;
    @JsonProperty("userClient")
    private String userClient;
    @JsonProperty("companyClient")
    private String companyClient;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getOnDelivery() {
        return onDelivery;
    }

    public void setOnDelivery(String onDelivery) {
        this.onDelivery = onDelivery;
    }

    public String getUserSupplier() {
        return userSupplier;
    }

    public void setUserSupplier(String userSupplier) {
        this.userSupplier = userSupplier;
    }

    public String getCompanySupplier() {
        return companySupplier;
    }

    public void setCompanySupplier(String companySupplier) {
        this.companySupplier = companySupplier;
    }

    public String getMerchandiseId() {
        return merchandiseId;
    }

    public void setMerchandiseId(String merchandiseId) {
        this.merchandiseId = merchandiseId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getUserClient() {
        return userClient;
    }

    public void setUserClient(String userClient) {
        this.userClient = userClient;
    }

    public String getCompanyClient() {
        return companyClient;
    }

    public void setCompanyClient(String companyClient) {
        this.companyClient = companyClient;
    }
}
