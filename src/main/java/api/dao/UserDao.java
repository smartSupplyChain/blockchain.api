package api.dao;

import api.authentication.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserDao  extends CrudRepository<User, String> {
    Optional<User> findByEmail(String email);
}
