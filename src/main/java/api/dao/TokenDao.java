package api.dao;

import api.authentication.model.Token;
import api.authentication.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TokenDao extends CrudRepository<Token, Integer> {
        Optional<Token> findByUser(User user);
}